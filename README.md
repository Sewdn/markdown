# Markdown editor
## Tracks changes to markdown file, publishes and broadcasts markup.
* you can make changes to a markdown file from
    * each and every browser that connects
    * directly on the central file itself

* every (pushed) change is broadcasted to all connected clients
    * use CTRL-enter to push your changes

## Installation and first run
    git clone https://Sewdn@bitbucket.org/Sewdn/markdown.git
    cd markdown
    npm install
    node server.js
load application through [http://localhost:3000](http://localhost:3000)

## checkout [Wikipedia](http://en.wikipedia.org/wiki/Markdown) for more info on markdown