var express = require('express');
var inotify = require('inotify-plusplus').create(true);
var md = require("node-markdown").Markdown;
var fs = require('fs');
var io = require('socket.io');

var app, inotify, directive, options, source, output, sockets;
app = express.createServer();
sockets = [];

var connect = require('express/node_modules/connect'),
  parseCookie = connect.utils.parseCookie,
  MemoryStore = connect.middleware.session.MemoryStore,
  store;

var path = './testfiles';
var file = 'test.md';

updateMarkdown = function(){
  fs.readFile(path + '/' + file, 'utf8', function (err, data) {
    if (err) throw err;
    source = data;
    output = md(source);
    for(var key in sockets){
      sockets[key].emit('refresh', source, output);
    }
  });
};

directive = (function() {
    // private variables
    var count = 0,
      validate_watch,
      close,
      cookies = {};
    close = function (ev) {
      //check for changes
      if(ev.name == file){
        updateMarkdown();
      }
    };

    return {
      close_write: close
    };
}());

options = {
    allow_bad_paths: true // (default false) don't throw an error if the path to watch doesn't exist
};

inotify.watch(directive, path, {});

app.configure(function () {
  app.set('view engine', 'jade');
  app.set('view options', {layout: false});
  app.use(express.cookieParser());
  app.use(express.session({
    secret: 'secret',
    key: 'express.sid',
    store: store = new MemoryStore()
  }));
});

io.listen(app).set('authorization', function (data, accept) {
  if (!data.headers.cookie) {
    return accept('No cookie transmitted.', false);
  }

  data.cookie = parseCookie(data.headers.cookie);
  data.sessionID = data.cookie['express.sid'];

  store.load(data.sessionID, function (err, session) {
    if (err || !session) return accept('Error', false);

    data.session = session;
    return accept(null, true);
  });
}).sockets.on('connection', function (socket) {
  sockets.push(socket);
  var sess = socket.handshake.session;
  socket.log.info(
    'a socket with sessionID',
    socket.handshake.sessionID,
    'connected'
  );
  updateMarkdown();
  socket.on('change source', function (src) {
    socket.log.info(
      'source was changed', src
    );
    fs.writeFile(path + '/' + file, src, 'utf8', function (err, data) {
      if (err) throw err;
      socket.log.info(
        'source was written', path + '/' + file
      );
    });
  });
});
 
app.get('/', function(req, res) {
  //req.session.value
  res.render('index', {markdown: output, source: source});
});

app.listen(3000);